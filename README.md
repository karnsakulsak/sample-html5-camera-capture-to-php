# References
* [HTML5 Camera Capture into Canvas]( https://davidwalsh.name/browser-camera)
* [Send canvas image to server](https://stackoverflow.com/questions/13198131/how-to-save-an-html5-canvas-as-an-image-on-a-server)
* [Save base64-encoded dataURL to file](https://stackoverflow.com/questions/1532993/i-have-a-base64-encoded-png-how-do-i-write-the-image-to-a-file-in-php)